# ptf2019temp

## Introduction
This is trial tech stack for using 'React based static site generater called Gatsby and firebase to build new portfolio site.
See how powerful the static site generator. If this is great, will think about transferring wp to these tech.

### Plan
Check and see how easy and powerful to set up this site are. And also check building time except for contents.
According to the information below, firebase are faster loading time than Netlify.
Although you aim for set up the site as cheap as you can, you should go with netlify. 
Whenever the time I get, I would try setting up Netlify one.

For static generator to choose, my first plan to implement option was Hugo, but for some reason, go lang can not applicable to this pc. 

- [Refsite](https://techblog.kayac.com/netlify-vs-firebase-2018)

Following site are the set up procedure that I tried.
- [Ref1](https://qiita.com/kei0425/items/91af9616dcaf4326c251)
- [Ref2](https://honmushi.com/2018/11/26/createblog/)

### Procedure
#### Installation
1. Install Git, npm, gatsby-cli.
2. Pick up one suitable anonymous template from gatsby starter [RefURL](https://www.gatsbyjs.org/starters/codebushi/gatsby-starter-dimension/)
    - NOTE: Individual template uses their preferred technology, choose carefully as you customize.
3. Set up the gatsby template to your local dir your ``` gatsby {your local root dir} {github url https://github.com...} ``` 
4. Test deployment on my local environment, ``` gatsby develop ``` 
5. Access to ``` localhost:8000 ``` on your browser
6. Change your front-end codes like HTML/CSS/JS as your preferred UI, each file location is below 
    - HTML is under pages dirctory but all the parts supposedly written in HTML is written in JS codes
      Individual JS codes are set in different directory{ index.js@root/src/pages/, main.js, footer.js@root/src/components}
      ** Check the images below to understand more
    - CSS amd images is under {root/src/assets} * this template uses SCSS. if you wanna use scss you need to install plugin using npm
    - Front UI controlling JS is under {}
    - config file is under the root directory
    ** As a result of me inspecting structure then, this cofiguration might just work for this templates.


    
#### Saving your files
This mostly depends on git process.
Firebase never let you download files, but you can roll back the files
Therefore, git process are mandatory.

#### Finishing up with gatsby commands
gatsby usually require you to 
1. ``` gatsby develop ``` is to test building a site
2. ``` gatsby serve ``` is to check if this is ok on compilation
3. ``` gatsby build ``` is to copletely compile and build. This process is necessary for uploading server

#### Uploading on Firebase 
this is the flow to upload file to firebase. you are expected to get your site and gatsby comilation done.
1. ``` firebase init ``` is to initialize your working directory and set up on firebase
2. ``` firebase serve ``` is to check if your site displaying ok on your local host.
3. ``` firebase deploy ``` is to deploy on your file to firebase server
``` ``` is the CLI commands running on your terminal if not specified.

These are the overall process but loughly.
If you follow these steps above, you are almost good to go.
Might forget some of the steps x)


